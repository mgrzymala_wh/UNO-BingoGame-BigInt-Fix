USE [BusinessSemanticLayer]
GO


SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Marek Grzymala
-- Create date: 2020-06-05T14:42
-- Description:	helper sp to insert into Target tables
-- =============================================

CREATE OR ALTER PROCEDURE [dbo].[usp_InsertInto_Target_Table_BingoGame_NEW]
      @SourceSchemaName NVARCHAR(256)
    , @TargetSchemaName NVARCHAR(256)
    , @SourceTableName NVARCHAR(256)
    , @TargetTableName NVARCHAR(256)
    , @FromDate DATETIME
    , @ToDate DATETIME
    --, @SourcePartitionNumber INT
    --, @TargetLowerPartitionBoundary DATETIME
    --, @SourceUpperPartitionBoundary DATETIME
    --, @SourceRows BIGINT
    --, @SourceUsedMB NUMERIC

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SET XACT_ABORT ON;

--------------------------------------
DECLARE @SourceSchemaNameQt NVARCHAR(256) = QUOTENAME(@SourceSchemaName)
DECLARE @TargetSchemaNameQt NVARCHAR(256) = QUOTENAME(@TargetSchemaName)
DECLARE @SourceTableNameQt NVARCHAR(256) = QUOTENAME(@SourceTableName)
DECLARE @TargetTableNameQt NVARCHAR(256) = QUOTENAME(@TargetTableName)
DECLARE @Sql NVARCHAR(MAX)
DECLARE @ColumnListSource NVARCHAR(MAX)
DECLARE @ColumnListTarget NVARCHAR(MAX)
DECLARE @ErrMsg VARCHAR(MAX)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
; WITH cte AS (
    SELECT DISTINCT
                '  ['+col.column_name+'] '    AS [ColumnName]  
    FROM          INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN    INFORMATION_SCHEMA.TABLES   tbl ON tbl.TABLE_NAME = col.TABLE_NAME
    WHERE         
    tbl.table_schema            = @SourceSchemaName AND 
    tbl.table_name              = @SourceTableName          
),
colListNumbered AS (
SELECT 
    ROW_NUMBER() OVER (ORDER BY cte.ColumnName) AS [ColumnNumber],
    cte.ColumnName
FROM cte
),
mcv AS (
    SELECT MAX(colListNumbered.[ColumnNumber]) AS MaxColNrVal FROM colListNumbered
),
colListFinal AS (
    SELECT  -- adding a comma to all columns except for the last one:
            CASE WHEN cln.[ColumnNumber] < (SELECT TOP 1 (mcv.MaxColNrVal) FROM mcv) THEN cln.[ColumnName]+',' ELSE cln.[ColumnName] END AS [ColumnNames]
    FROM    colListNumbered cln
)
SELECT @ColumnListSource = 
(
     -- flatten all rows into a single row:
     SELECT SUBSTRING([ColumnNames], 0, 9999) 
     FROM colListFinal FOR XML PATH('') 
)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
; WITH cte AS (
    SELECT DISTINCT
                '  ['+col.column_name+'] '    AS [ColumnName]  
    FROM          INFORMATION_SCHEMA.COLUMNS  col
    INNER JOIN    INFORMATION_SCHEMA.TABLES   tbl ON tbl.TABLE_NAME = col.TABLE_NAME
    WHERE         
    tbl.table_schema            = @TargetSchemaName AND 
    tbl.table_name              = @TargetTableName          
),
colListNumbered AS (
SELECT 
    ROW_NUMBER() OVER (ORDER BY cte.ColumnName) AS [ColumnNumber],
    cte.ColumnName
FROM cte
),
mcv AS (
    SELECT MAX(colListNumbered.[ColumnNumber]) AS MaxColNrVal FROM colListNumbered
),
colListFinal AS (
    SELECT  -- adding a comma to all columns except for the last one:
            CASE WHEN cln.[ColumnNumber] < (SELECT TOP 1 (mcv.MaxColNrVal) FROM mcv) THEN cln.[ColumnName]+',' ELSE cln.[ColumnName] END AS [ColumnNames]
    FROM    colListNumbered cln
)
SELECT @ColumnListTarget = 
(
     -- flatten all rows into a single row:
     SELECT SUBSTRING([ColumnNames], 0, 9999)
     FROM colListFinal FOR XML PATH('') 
)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
IF ((@ColumnListSource IS NULL OR @ColumnListTarget IS NULL) OR (@ColumnListSource <> @ColumnListTarget))
BEGIN
      SET @ErrMsg = 'Error executing: ' + COALESCE(OBJECT_NAME(@@PROCID), 'Unknown procedure') + CHAR(13) + '
      Lists of columns collected for tables: '+@SourceSchemaNameQt+'.'+@SourceTableNameQt+' and '+@TargetSchemaNameQt+'.'+@TargetTableNameQt+' do not match or are emty'+ CHAR(13)
      ;THROW 99001, @ErrMsg, 1;
END
-----------------------------------------------------------------------------------------------------------------------------------------------------------------

PRINT(CONCAT('Inserting from source table: ', @SourceSchemaNameQt, '.', @SourceTableNameQt
, ' into table: ', @TargetSchemaNameQt, '.', @TargetTableNameQt, ' with date range from: ', +CONVERT(NVARCHAR(64), @FromDate), ' to: ', CONVERT(NVARCHAR(64), @ToDate)))

SET @Sql = N'SET IDENTITY_INSERT [dbo].'+@TargetTableNameQt+' ON;'
--SET @Sql = @Sql+N'
--       INSERT INTO [dbo].'+@TargetTableNameQt+'
--        (
--         '+@ColumnListTarget+'
--        )
--        SELECT 
--         '+@ColumnListSource+'
--        FROM [dbo].'+@SourceTableNameQt+' WHERE SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @FromDate)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @ToDate)+''';'
SET @Sql = @Sql+N'
       INSERT INTO [dbo].'+@TargetTableNameQt+'
        (
         '+@ColumnListTarget+'
        )
        SELECT 
         '+@ColumnListSource+'
        FROM [dbo].'+@SourceTableNameQt+
        --' WITH (INDEX(IX_NCL_dbo_RecipientDeliveryLog_Current_SystemUpdatedDt)) 
        ' WHERE SystemUpdatedDt >= '''+CONVERT(NVARCHAR(64), @FromDate)+''' AND SystemUpdatedDt < '''+CONVERT(NVARCHAR(64), @ToDate)+''';'
--PRINT(@Sql)
EXEC sp_executesql @Sql

-----------------------------------------------------------------------------------------------------------------------------------------------------------------
END
GO

