USE [BusinessSemanticLayer]
GO

SELECT [Id]
      ,[SourceTableName]
      ,[TargetTableName]
      ,[TargetPartitionNumber]
      ,[SourceRows]
      ,[TargetRows]
      ,[TargetUsedMB]
      ,[TargetLowerPartitionBoundary]
      ,[TargetUpperPartitionBoundary]
      ,[migrated]
      , CONVERT(TIME(0),[Duration]) AS [Duration]
  FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking]

SET NOCOUNT ON
SET XACT_ABORT ON

DECLARE 
  @Id                              INT
, @MaxId                           INT
, @_SourceRows                     BIGINT
, @_TargetRows                     BIGINT
, @_TargetPartitionNumber          INT
, @_TargetUpperPartitionBoundary   DATETIME
, @Sql                             VARCHAR(1024)
, @_SourceSchemaName               NVARCHAR(256) = 'dbo' -- change this to match the source table name
, @_TargetSchemaName               NVARCHAR(256) = 'dbo'
, @_SourceTableName                NVARCHAR(256) = 'BingoGame' -- change this to match the source table name
, @_TargetTableName                NVARCHAR(256) = 'BingoGame_NEW'
, @StopAtDate                      DATETIME = '2019-02-01 00:00:00.000' -- for @_TargetTableName = 'RecipientDeliveryLog_Archive' set this valuie to: DATEADD(DAY, -30, GETDATE())
, @Duration_Start                  DATETIME
, @Duration_Stop                   DATETIME
, @Duration                        DATETIME


SELECT @Id = MIN([Id]) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE [migrated] = 0
SELECT @MaxId = MAX([Id]) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking]

IF (SELECT COUNT(*) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE [migrated] = 1) > 0
BEGIN
    SET @_TargetUpperPartitionBoundary = COALESCE((SELECT MAX([TargetUpperPartitionBoundary]) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE [migrated] = 1), CAST('1753-1-1' AS DATETIME))
END
ELSE
BEGIN
    SET @_TargetUpperPartitionBoundary = COALESCE((SELECT MIN([TargetUpperPartitionBoundary]) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking]), CAST('1753-1-1' AS DATETIME))
END

SELECT @Sql = 'ALTER TABLE ['+@_TargetSchemaName+'].['+@_TargetTableName+'] DROP CONSTRAINT IF EXISTS [DF_dbo_'+@_TargetTableName+'_SystemUpdatedDt]'
PRINT 'Processing table: ['+@_TargetSchemaName+'].['+@_TargetTableName+'], dropping constraint: [DF_dbo_'+@_TargetTableName+'_SystemUpdatedDt]'
EXEC(@Sql)

WHILE ((@Id <= @MaxId) AND (@_TargetUpperPartitionBoundary <= @StopAtDate))
BEGIN
        
        SELECT 
                @_TargetPartitionNumber = [TargetPartitionNumber]
                ,@_TargetUpperPartitionBoundary = [TargetUpperPartitionBoundary] 
        FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE Id = @Id
        
        --SELECT @_SourceRows = COUNT_BIG(*) FROM [BusinessSemanticLayer].[dbo].[RecipientDeliveryLog_Source] WHERE SystemUpdatedDt >= DATEADD(DAY, -1, @_TargetUpperPartitionBoundary) AND SystemUpdatedDt < @_TargetUpperPartitionBoundary
        PRINT(CHAR(13)+'------------------------------- Processing record: '+CONVERT(NVARCHAR(32), @Id)+' of: '+CONVERT(NVARCHAR(32), @MaxId)+' ------------------------------- ')
        SELECT @Duration_Start = GETDATE()

        DECLARE @_FromDate DATETIME = DATEADD(DAY, -1, @_TargetUpperPartitionBoundary)
        SELECT @_FromDate = DATEADD(DAY, -1, @_TargetUpperPartitionBoundary)
        
        EXEC [dbo].[usp_GetCountFromSource]
                  @SourceSchemaName = @_SourceSchemaName
                , @SourceTableName  = @_SourceTableName
                , @FromDate         = @_FromDate
                , @ToDate           = @_TargetUpperPartitionBoundary
                , @Count            = @_SourceRows OUTPUT
        
        PRINT(CONCAT('Updating [ArchiveData_Migration_Tracking] partition ', @_TargetPartitionNumber, ', Upper Partition Boundary: ', CAST(@_TargetUpperPartitionBoundary AS NVARCHAR(32)), ', with row count of: ', @_SourceRows))
        UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET SourceRows = @_SourceRows WHERE [Id] = @Id
        -----------------------------------
        -- adjust the [DF_dbo_RecipientDeliveryLog_Archive_SystemUpdatedDt] constraint on [RecipientDeliveryLog_Archive]: move the HighestValue-Constraint up to allow newer records to be merged-in
        SELECT @Sql =  'ALTER TABLE ['+@_TargetSchemaName+'].['+@_TargetTableName+'] DROP CONSTRAINT IF EXISTS [DF_dbo_'+@_TargetTableName+'_SystemUpdatedDt]'
        EXEC(@Sql)
        SELECT @Sql = 'ALTER TABLE ['+@_TargetSchemaName+'].['+@_TargetTableName+'] WITH CHECK ADD CONSTRAINT [DF_dbo_'+@_TargetTableName+'_SystemUpdatedDt] CHECK (SystemUpdatedDt < '''+CONVERT(NVARCHAR(32), @_TargetUpperPartitionBoundary)+''')'
        PRINT 'Processing table: ['+@_TargetSchemaName+'].['+@_TargetTableName+'], adjusting HighestValue-Constraint to: '+CONVERT(NVARCHAR(32), @_TargetUpperPartitionBoundary)
        EXEC(@Sql)
        
        EXEC [dbo].[usp_InsertInto_Target_Table_BingoGame_NEW]
                @SourceSchemaName   = @_SourceSchemaName
                , @TargetSchemaName = @_TargetSchemaName
                , @SourceTableName  = @_SourceTableName
                , @TargetTableName  = @_TargetTableName
                , @FromDate         = @_FromDate
                , @ToDate           = @_TargetUpperPartitionBoundary
   
        EXEC [dbo].[usp_GetCountFromSource]
                  @SourceSchemaName = @_TargetSchemaName
                , @SourceTableName  = @_TargetTableName --'RecipientDeliveryLog_Archive'
                , @FromDate         = @_FromDate
                , @ToDate           = @_TargetUpperPartitionBoundary
                , @Count            = @_TargetRows OUTPUT
                
        IF (@_TargetRows = @_SourceRows)
        BEGIN
        PRINT('------------------------------- End of Processing record: '+CONVERT(NVARCHAR(32), @Id)+' of: '+CONVERT(NVARCHAR(32), @MaxId)+' ------------------------------- '+CHAR(13))
        SELECT @Duration_Stop = GETDATE()
        SELECT @Duration = @Duration_Stop - @Duration_Start

        -- mark the partition as migrated:
            UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET 
                   [SourceTableName] = @_SourceTableName
                  ,[TargetTableName] = @_TargetTableName
                  ,[TargetRows] = @_TargetRows 
                  ,[migrated] = 1
                  ,[Duration] = @Duration
            WHERE [Id] = @Id
            -- set the values for next iteration:
            SELECT @Id = MIN([Id]) FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE [migrated] = 0
            SELECT 
                    @_TargetPartitionNumber = [TargetPartitionNumber],       
                    @_SourceRows = [SourceRows],                  
                    --@_TargetUsedMB = [TargetUsedMB],                
                    @_TargetUpperPartitionBoundary = [TargetUpperPartitionBoundary]
            FROM    [DMOperations].[dbo].[ArchiveData_Migration_Tracking] WHERE Id = @Id
        END
        
END


SELECT [Id]
      ,[SourceTableName]
      ,[TargetTableName]
      ,[TargetPartitionNumber]
      ,[SourceRows]
      ,[TargetRows]
      ,[TargetUsedMB]
      ,[TargetLowerPartitionBoundary]
      ,[TargetUpperPartitionBoundary]
      ,[migrated]
      , CONVERT(TIME(0),[Duration]) AS [Duration]
  FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking]



/*

-- reset migrated flag and SourceRows
BEGIN
    UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET [SourceRows] = 0 WHERE [SourceTableName] = 'RecipientDeliveryLog_Current'
    UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET [migrated] = 0 WHERE Id > 152 --[SourceTableName] = 'RecipientDeliveryLog_Current'
    UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET [SourceTableName] = NULL WHERE [SourceTableName] = 'RecipientDeliveryLog_Current'--IS NOT NULL
    UPDATE [DMOperations].[dbo].[ArchiveData_Migration_Tracking] SET [TargetTableName] = NULL WHERE [TargetTableName] = 'RecipientDeliveryLog_Current' --IS NOT NULL
    TRUNCATE TABLE [BusinessSemanticLayer].[dbo].[RecipientDeliveryLog_Archive]
    TRUNCATE TABLE [BusinessSemanticLayer].[dbo].[RecipientDeliveryLog_Current]
END
*/

