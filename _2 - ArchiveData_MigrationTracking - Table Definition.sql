SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET XACT_ABORT ON
GO

USE [DMOperations]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ArchiveData_Migration_Tracking]') AND type IN (N'U'))
DROP TABLE [dbo].[ArchiveData_Migration_Tracking]
GO

CREATE TABLE [dbo].[ArchiveData_Migration_Tracking](
    [Id]                             INT IDENTITY NOT NULL PRIMARY KEY,
	[SourceTableName]                NVARCHAR(256) NULL,
    [TargetTableName]                NVARCHAR(256) NULL,
    [TargetPartitionNumber]          INT NOT NULL UNIQUE, -- this must be unique, otherwise partition migration process will try to double-migrate the same partitions
	[SourceRows]                     BIGINT NOT NULL,
	[TargetRows]                     BIGINT NOT NULL DEFAULT 0,
	[TargetUsedMB]                   NUMERIC(27, 6) NULL,
	[TargetLowerPartitionBoundary]   DATETIME NULL,
	[TargetUpperPartitionBoundary]   DATETIME NULL,
    [migrated]                       BIT NOT NULL DEFAULT 0,
    [Duration]                       DATETIME NULL
) ON [PRIMARY]
GO

USE [BusinessSemanticLayer]
GO

DECLARE @ArchiveTableName NVARCHAR(256) = 'BingoGame_NEW'
DECLARE @ArchiveStartingPoint DATETIME = '2019-07-01'

; WITH cte1 AS (
SELECT

	  prt.[partition_number]                                AS [partition_number]
	, CASE WHEN ix.index_id < 2 THEN prt.rows ELSE 0 END    AS [Rows]
	, rv.[value]                                            AS [UpperPartitionBoundary]
    , pf.[boundary_value_on_right]                          AS [boundary_value_on_right]


FROM 
	sys.partitions prt
	INNER JOIN  sys.indexes ix                      ON ix.object_id = prt.object_id AND ix.index_id = prt.index_id                                          
	INNER JOIN  sys.tables st                       ON prt.object_id = st.object_id                                                                         
	INNER JOIN  sys.index_columns ic                ON (ic.partition_ordinal > 0 AND ic.index_id = ix.index_id AND ic.object_id = st.object_id)             
	INNER JOIN  sys.columns c                       ON (c.object_id = ic.object_id AND c.column_id = ic.column_id)                                          
	INNER JOIN  sys.data_spaces ds                  ON ds.data_space_id = ix.data_space_id                                                                  
	LEFT JOIN   sys.partition_schemes ps            ON ps.data_space_id = ix.data_space_id                                                                  
	LEFT JOIN   sys.partition_functions pf          ON pf.function_id = ps.function_id                                                                      
	LEFT JOIN   sys.partition_range_values rv       ON rv.function_id = pf.function_id AND rv.boundary_id = prt.partition_number                            

WHERE 
        st.name = @ArchiveTableName
AND     ix.index_id = 1 -- select min index_id from sys.indexes where column is partitioned by the partitioning scheme and is_disabled = 0

)

INSERT INTO  [DMOperations].[dbo].[ArchiveData_Migration_Tracking] (
             [TargetPartitionNumber]       
            ,[SourceRows]                               
            ,[TargetLowerPartitionBoundary]
            ,[TargetUpperPartitionBoundary]
            ,[migrated]    
)
SELECT      DISTINCT
             cte1.[partition_number]
            ,cte1.[Rows]
            ,CONVERT(DATETIME, ISNULL(LAG(cte1.[UpperPartitionBoundary]) OVER (ORDER BY cte1.partition_number), CAST('1753-1-1' AS DATETIME))) AS [LowerPartitionBoundary]
            ,CONVERT(DATETIME, cte1.[UpperPartitionBoundary]) AS [UpperPartitionBoundary]

            ,0    AS [migrated]
FROM        cte1
WHERE       CONVERT(DATETIME, ISNULL(cte1.[UpperPartitionBoundary], CONVERT(DATETIME, '1753-1-1'))) >= @ArchiveStartingPoint
GO


SELECT * FROM [DMOperations].[dbo].[ArchiveData_Migration_Tracking] 