USE [BusinessSemanticLayer]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE IF EXISTS [dbo].[BingoGame_NEW]

CREATE TABLE [dbo].[BingoGame_NEW](
	[BingoGameId] BIGINT IDENTITY(1,1) NOT NULL,
	[BingoGameCurrencyId] [int] NOT NULL,
	[CustomerAccountId] [int] NOT NULL,
	[BingoBrandId] [int] NOT NULL,
	[DomainId] [int] NOT NULL,
	[SourceSystemId] [int] NULL,
	[GamePlayId] [bigint] NOT NULL,
	[BingoTransactionId] [varchar](50) NULL,
	[BingoUserId] [varchar](255) NULL,
	[BingoGameName] [varchar](50) NULL,
	[BingoStake] [decimal](18, 8) NOT NULL,
	[BingoStakeGBP] [decimal](18, 8) NOT NULL,
	[BingoStakeUSD] [decimal](18, 8) NOT NULL,
	[BingoStakeEUR] [decimal](18, 8) NOT NULL,
	[BingoStakeBonus] [decimal](18, 8) NOT NULL,
	[BingoStakeBonusGBP] [decimal](18, 8) NOT NULL,
	[BingoStakeBonusUSD] [decimal](18, 8) NOT NULL,
	[BingoStakeBonusEUR] [decimal](18, 8) NOT NULL,
	[BingoWin] [decimal](18, 8) NOT NULL,
	[BingoWinGBP] [decimal](18, 8) NOT NULL,
	[BingoWinUSD] [decimal](18, 8) NOT NULL,
	[BingoWinEUR] [decimal](18, 8) NOT NULL,
	[BingoReturnedBonus] [decimal](18, 8) NOT NULL,
	[BingoReturnedBonusGBP] [decimal](18, 8) NOT NULL,
	[BingoReturnedBonusUSD] [decimal](18, 8) NOT NULL,
	[BingoReturnedBonusEUR] [decimal](18, 8) NOT NULL,
	[GameStartedDtTm] [datetime] NOT NULL,
	[GameEndedDtTm] [datetime] NOT NULL,
	[DtStaked] [datetime] NOT NULL,
	[MasterGamePlayId] [decimal](18, 0) NULL,
	[LoyaltyPointsEarned] [decimal](18, 8) NULL,
	[BingoStakedPending] [decimal](18, 8) NOT NULL,
	[BingoStakedPendingGBP] [decimal](18, 8) NOT NULL,
	[BingoStakedPendingUSD] [decimal](18, 8) NOT NULL,
	[BingoStakedPendingEUR] [decimal](18, 8) NOT NULL,
	[BingoReturnedPending] [decimal](18, 8) NOT NULL,
	[BingoReturnedPendingGBP] [decimal](18, 8) NOT NULL,
	[BingoReturnedPendingUSD] [decimal](18, 8) NOT NULL,
	[BingoReturnedPendingEUR] [decimal](18, 8) NOT NULL,
	[InformationSourceId] [int] NOT NULL,
	[SystemCreatedDt] [datetime] NOT NULL,
	[SystemUpdatedDt] [datetime] NOT NULL
) ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE CLUSTERED COLUMNSTORE INDEX [IX_C_Col_dbo_BingoGame] ON [dbo].[BingoGame_NEW] WITH (DROP_EXISTING = OFF, COMPRESSION_DELAY = 0, DATA_COMPRESSION = COLUMNSTORE_ARCHIVE)
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD CONSTRAINT [PK_dbo_BingoGame_BingoGameId] PRIMARY KEY NONCLUSTERED 
(
	[BingoGameId] ASC,
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE NONCLUSTERED INDEX [IX_NCL_dbo_BingoGame_BingoTransactionId] ON [dbo].[BingoGame_NEW]
(
	[BingoTransactionId] ASC,
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
ON [ps_daily_date]([SystemUpdatedDt])
GO

CREATE NONCLUSTERED INDEX [IX_NCL_dbo_BingoGame_CustomerAccountId] ON [dbo].[BingoGame_NEW]
(
	[CustomerAccountId]  ASC,
	[SystemUpdatedDt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) 
ON [ps_daily_date]([SystemUpdatedDt])
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStake]  DEFAULT ((0)) FOR [BingoStake]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeGBP]  DEFAULT ((0)) FOR [BingoStakeGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeUSD]  DEFAULT ((0)) FOR [BingoStakeUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeEUR]  DEFAULT ((0)) FOR [BingoStakeEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeBonus]  DEFAULT ((0)) FOR [BingoStakeBonus]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeBonusGBP]  DEFAULT ((0)) FOR [BingoStakeBonusGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeBonusUSD]  DEFAULT ((0)) FOR [BingoStakeBonusUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakeBonusEUR]  DEFAULT ((0)) FOR [BingoStakeBonusEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoWin]  DEFAULT ((0)) FOR [BingoWin]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoWinGBP]  DEFAULT ((0)) FOR [BingoWinGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoWinUSD]  DEFAULT ((0)) FOR [BingoWinUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoWinEUR]  DEFAULT ((0)) FOR [BingoWinEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedBonus]  DEFAULT ((0)) FOR [BingoReturnedBonus]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedBonusGBP]  DEFAULT ((0)) FOR [BingoReturnedBonusGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedBonusUSD]  DEFAULT ((0)) FOR [BingoReturnedBonusUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedBonusEUR]  DEFAULT ((0)) FOR [BingoReturnedBonusEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakedPending]  DEFAULT ((0)) FOR [BingoStakedPending]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakedPendingGBP]  DEFAULT ((0)) FOR [BingoStakedPendingGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakedPendingUSD]  DEFAULT ((0)) FOR [BingoStakedPendingUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoStakedPendingEUR]  DEFAULT ((0)) FOR [BingoStakedPendingEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedPending]  DEFAULT ((0)) FOR [BingoReturnedPending]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedPendingGBP]  DEFAULT ((0)) FOR [BingoReturnedPendingGBP]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedPendingUSD]  DEFAULT ((0)) FOR [BingoReturnedPendingUSD]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_BingoReturnedPendingEUR]  DEFAULT ((0)) FOR [BingoReturnedPendingEUR]
GO

ALTER TABLE [dbo].[BingoGame_NEW] ADD  CONSTRAINT [DF_dbo_BingoGame_NEW_InformationSourceId]  DEFAULT ((2)) FOR [InformationSourceId]
GO

ALTER TABLE [dbo].[BingoGame_NEW]  WITH CHECK ADD  CONSTRAINT [FK_dbo_BingoGame_NEW_BingoBrandId] FOREIGN KEY([BingoBrandId])
REFERENCES [dbo].[Brand] ([BrandId])
GO

ALTER TABLE [dbo].[BingoGame_NEW] CHECK CONSTRAINT [FK_dbo_BingoGame_NEW_BingoBrandId]
GO

ALTER TABLE [dbo].[BingoGame_NEW]  WITH CHECK ADD  CONSTRAINT [FK_dbo_BingoGame_NEW_BingoGameCurrencyId] FOREIGN KEY([BingoGameCurrencyId])
REFERENCES [dbo].[Currency] ([CurrencyId])
GO

ALTER TABLE [dbo].[BingoGame_NEW] CHECK CONSTRAINT [FK_dbo_BingoGame_NEW_BingoGameCurrencyId]
GO

ALTER TABLE [dbo].[BingoGame_NEW]  WITH CHECK ADD  CONSTRAINT [FK_dbo_BingoGame_NEW_CustomerAccountId] FOREIGN KEY([CustomerAccountId])
REFERENCES [dbo].[CustomerAccount] ([CustomerAccountId])
GO

ALTER TABLE [dbo].[BingoGame_NEW] CHECK CONSTRAINT [FK_dbo_BingoGame_NEW_CustomerAccountId]
GO

ALTER TABLE [dbo].[BingoGame_NEW]  WITH CHECK ADD  CONSTRAINT [FK_dbo_BingoGame_NEW_DomainId] FOREIGN KEY([DomainId])
REFERENCES [dbo].[Domain] ([DomainId])
GO

ALTER TABLE [dbo].[BingoGame_NEW] CHECK CONSTRAINT [FK_dbo_BingoGame_NEW_DomainId]
GO

EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'BingoGameCurrencyId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'BingoGameCurrencyId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'BingoGameCurrencyId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'BingoGameCurrencyId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_information_type_id', @value=N'C44193E1-0E58-4B2A-9001-F7D6E7BC1373' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_information_type_name', @value=N'Financial' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_id', @value=N'331F0B13-76B5-2F1B-A77B-DEF5A73C73C2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
GO

EXEC sys.sp_addextendedproperty @name=N'sys_sensitivity_label_name', @value=N'Confidential' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'BingoGame_NEW', @level2type=N'COLUMN',@level2name=N'CustomerAccountId'
GO

